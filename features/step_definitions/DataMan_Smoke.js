var action = require('../../../Keywords');
var Objects = require(__dirname + '/../../objects/DataMan_locators.js');
//var robot = require('robot-js');
var data = require('../../test_data/DataMan_TestData.js');
//var robot1
// 23 = require('robotjs');
var browser;

module.exports = function () {

    this.Given(/^User Navigate to DataMan Home Page$/, function (browser) {
        var URL;
        action.initializePageObjects(browser, function () {
            if (data.TestingEnvironment == "QAI")
                URL = data.urlQAI;
            else if (data.TestingEnvironment == "CSO")
                URL = data.urlCSO;
            else if (data.TestingEnvironment == "DEVI")
                URL = data.urlDEVI;
            else {
                URL = data.urlPROD;
            }
            browser.maximizeWindow()
                .deleteCookies()
                .url(URL);
            browser.pause(data.shortwaittime);

            //extra added steps to comment
            //----------------------------------
            // browser.pause(10000);
            // robot123.typeString('xxxxxxxxx');
            // browser.pause(5000);
            // robot123.keyTap("tab");
            // browser.pause(5000);
            // robot123.typeString(xxxxxxxxx');
            // browser.pause(5000);
            // robot123.keyTap("enter");
            // browser.pause(10000);
            // console.log("after enter tap");
            // //---------------------------


        });
    });


    this.Then(/^Verify Mercer logo at top left$/, function (browser) {
        action.initializePageObjects(browser, function () {
            browser.pause(data.averagepause);
            action.isDisplayed("HomePage|MercerLogo", function () {
            });
        });

    });


    this.Then(/^Verify Administration Foundation title$/, function (browser) {
        action.initializePageObjects(browser, function () {
            this.demoTest = function (browser) {
                browser.getTitle(function (title) {
                    this.assert.equal(typeof title, 'string');
                    this.assert.equal(title, 'Administration Foundation');
                });
            };
        });

    });
    this.Then(/^User navigates to Clients tab$/, function (browser) {
        action.initializePageObjects(browser, function () {

            browser.pause(data.averagepause);

            action.performClick('ParticipantSearchPage|ClientsTab', function () {

            });

        });

    });
    this.Then(/^User enters client name$/, function (browser) {
       // action.initializePageObjects(browser, function () {

            browser.pause(data.averagepause);
           // action.performClick("ParticipantSearchPage|ClientNm", function () {
                action.setText("ParticipantSearchPage|ClientNm_xpath", data.ClientName);
            //});
       // });

    });

    this.Then(/^User enters client name QALIFE$/, function (browser) {

            //action.performClick("ParticipantSearchPage|ClientNm", function(){
        browser.pause(data.shortwaittime);
action.setText("ParticipantSearchPage|ClientNm_xpath",data.ClientName1)
            //});

    });


    this.Then(/^User clicks on "([^"]*)"$/, function (browser, locator) {
        action.initializePageObjects(browser, function () {
            browser.pause(data.averagepause);
            action.performClick(locator, function () {
            });
        });
    });


    this.Then(/^User Verifies "([^"]*),([^"]*)" is present$/, function (browser, locator, abc) {
        action.initializePageObjects(browser, function () {
            browser.pause(data.averagepause);
            action.verifyText(locator, abc)


        });

    });
    this.Then(/^Verify Quick Search box displays at the top right on all AFBO pages$/, function (browser) {
        action.initializePageObjects(browser, function () {

            browser.pause(data.averagepause);
            action.performClick('TopNavQuickSearch|ClientsTab', function () {
                action.isDisplayed("PptSearchResultPage|QuickSearch", function () {
                    action.performClick('TopNavQuickSearch|ParticipantsTab', function () {
                        action.isDisplayed("PptSearchResultPage|QuickSearch", function () {
                            action.performClick('TopNavQuickSearch|AdministrationTab', function () {
                                action.isDisplayed("PptSearchResultPage|QuickSearch", function () {
                                });


                            });
                        });
                    });
                });
            });
        });
    });
    this.Then(/^User clicks on dropdown button$/, function (browser) {
        action.initializePageObjects(browser, function () {

            browser.pause(data.averagepause);

            action.performClick('PptSearchResultPage|Dropdown', function () {
            });

        });

    });
    this.Then(/^Verify Search button functionality by entering last name of the participant$/, function (browser) {
        action.initializePageObjects(browser, function () {

            browser.pause(data.averagepause);

            action.performClick('TopNavQuickSearch|ParticipantsTab', function () {
                action.setText("TopNavQuickSearch|PptLastName", data.LastName);
                action.setText("TopNavQuickSearch|CountryCode", data.CountryCode);
                action.performClick('TopNavQuickSearch|PptBtnSearch', function () {
                });

            });
        });
    });

    this.Then(/^User clicks on Search button on top right corner without entering any text$/, function (browser) {
        action.initializePageObjects(browser, function () {

            browser.pause(data.averagepause);

            action.performClick('PptSearchResultPage|SearchbtnParticipants', function () {
            });

        });

    });

    this.Then(/^User is able to see all records returned for the search category displayed in the drop-down list i.e upto Configurable limit$/, function (browser) {
        action.initializePageObjects(browser, function () {

            browser.pause(data.averagepause);

            action.isDisplayed('PptSearchResultPage|ResTable', function () {
            });
        });
    });

    this.Then(/^User selects a file with EM task$/, function (browser) {
        action.initializePageObjects(browser, function () {

            browser.pause(data.averagepause);

            action.performClick('IFSLSelectedFile|FA', function () {

                action.performClick('IFSLSelectedFile|PR', function () {
                    action.performClick('IFSLSelectedFile|BW', function () {
                        action.performClick('IFSLSelectedFile|BS', function () {
                            action.performClick('IFSLSelectedFile|BC', function () {
                                action.performClick('IFSLSelectedFile|NP', function () {
                                    action.performClick('IFSLSelectedFile|ApplyFiltersbtn', function () {
                                        browser.pause(data.averagepause);
                                    });
                                });
                            });
                        });
                    });

                });

            });
        });
    });
    this.Then(/^User selects a file$/, function (browser) {
        action.initializePageObjects(browser, function () {

            browser.pause(data.averagepause);

            action.performClick('IFSLSelectedFile|FA', function () {
                action.performClick('IFSLSelectedFile|PR', function () {
                    action.performClick('IFSLSelectedFile|BW', function () {
                        action.performClick('IFSLSelectedFile|BS', function () {
                            action.performClick('IFSLSelectedFile|BC', function () {
                                action.performClick('IFSLSelectedFile|NP', function () {
                                    action.performClick('IFSLSelectedFile|IP', function () {
                                        action.performClick('IFSLSelectedFile|Failed', function () {
                                            action.performClick('IFSLSelectedFile|ApplyFiltersbtn', function () {

                                                browser.pause(data.averagepause);
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });

                });

            });
        });
    });
    this.Then(/^User selects the Last 24 Hours filter$/, function (browser) {
        action.initializePageObjects(browser, function () {

            browser.pause(data.averagepause);

            action.performClick('FileControlsHeaderPage|TimePeriodFilter', function () {
                action.performClick('FileControlsHeaderPage|Last24Hrs', function () {
                });
            });
        });
    });
}
