Feature: Smoke Scenarios for DataMan

  @AFBO
  Scenario: Verify AFBO Pages
    Given User Navigate to DataMan Home Page
    Then User Verifies "HomePage|WelcomeTitle,Welcome mukund gramani" is present
    Then User Verifies "HomePage|LoginUsername,Mercer\mukund-gramani" is present
    Then Verify Mercer logo at top left
    Then Verify Administration Foundation title
