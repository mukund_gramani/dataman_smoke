Feature: Smoke Scenarios for DataMan

  @QuickSearch
  Scenario: Verify Quick Search functionality
    Given User Navigate to DataMan Home Page
    Then Verify Quick Search box displays at the top right on all AFBO pages
    Then User clicks on dropdown button
    Then User Verifies "TopNavQuickSearch|ClientsDD,Clients" is present
    Then User Verifies "TopNavQuickSearch|ParticipantsDD,Participants" is present
    Then User Verifies "TopNavQuickSearch|AdministratorsDD,Administrators" is present
    Then Verify Search button functionality by entering last name of the participant
    Then User Verifies "PptSearchResultPage|PptSearchRes,Mercer\mukund-gramani" is present
    Then User clicks on Search button on top right corner without entering any text
    Then User is able to see all records returned for the search category displayed in the drop-down list i.e upto Configurable limit



