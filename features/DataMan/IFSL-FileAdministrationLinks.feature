Feature: Smoke Scenarios for DataMan

  @IFSL
  Scenario: Verify IFSL Navigation Links
    Given User Navigate to DataMan Home Page
    When User navigates to Clients tab
    And User enters client name
    Then User clicks on "ParticipantSearchPage|Searchbtn"
    Then User clicks on "ParticipantSearchPage|QA62BC"
    Then User clicks on "ParticipantSearchPage|DMInboundFileStatus"
    Then User clicks on "ParticipantSearchPage|DMConfiguration"
    Then User Verifies "ParticipantSearchPage|DataManConfiguration,Data Management Configuration" is present
    Then User Verifies "ParticipantSearchPage|ClientTransmissionLocationList,Client Transmission Location List" is present
    Then User Verifies "ParticipantSearchPage|FileConfigList,File Configuration List" is present
    Then User Verifies "ParticipantSearchPage|PullServerConfigList,Pull Server Configuration List" is present
    Then User clicks on "ParticipantSearchPage|InboundFileControlPanel"
    Then User Verifies "Validations|IFCP,Inbound File Control Panel" is present
    Then User clicks on "ParticipantSearchPage|MergeTableDelete"
    Then User Verifies "Validations|MTD,Delete Records with Errors from Merge table" is present
    Then User clicks on "ParticipantSearchPage|AutomatedBWLoadStatusReport"
    Then User Verifies "Validations|ABWLSR,Automated BW Load Status Report" is present
    Then User clicks on "ParticipantSearchPage|IFSList"
    Then User Verifies "Validations|InboundFSList,Inbound File Status List" is present
    Then User clicks on "HomePage|HomeTab"
    Then User Verifies "HomePage|MercerCopyright,© Copyright 2018 Mercer" is present
    Then User Verifies "HomePage|RecentClients,Recent Clients" is present
    Then User Verifies "HomePage|RecentParticipants,Recent Participants" is present