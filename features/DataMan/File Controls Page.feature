Feature: Smoke Scenarios for DataMan

  @FileControls
  Scenario: Verify File Controls Header Page Details
    Given User Navigate to DataMan Home Page
    When User navigates to Clients tab
    And User enters client name QALIFE
    Then User clicks on "ParticipantSearchPage|Searchbtn"
    Then User clicks on "ParticipantSearchPage|QA62BC"
    Then User clicks on "SelectedFileLinks|DataManIFStatus"
    Then User selects a file
    Then User clicks on "IFSLSelectedFile|FileDetails"
    Then User clicks on "IFSLSelectedFile|ManageExceptionsbtn"
    Then User clicks on "FileControlsHeaderPage|FileCtrls"
    Then User Verifies "FileControlsHeaderPage|DateReceived,Date Received:" is present
    Then User Verifies "FileControlsHeaderPage|FileID,File ID:" is present
    Then User Verifies "FileControlsHeaderPage|FileName,File Name:" is present
    Then User Verifies "FileControlsHeaderPage|ConfigID,Config ID:" is present
    Then User Verifies "FileControlsHeaderPage|AltPath,Alt Path:" is present
    Then User Verifies "FileControlsHeaderPage|FileType,File Type:" is present
    Then User Verifies "FileControlsHeaderPage|Loaded,Loaded:" is present
    Then User Verifies "FileControlsHeaderPage|ProcessingBy,Processing By:" is present
    Then User Verifies "FileControlsHeaderPage|OriginalAmt,Original Amt:" is present
    Then User Verifies "FileControlsHeaderPage|LastUpdated,Last Updated:" is present
    Then User Verifies "FileControlsHeaderPage|EnabledApplications,Enabled Applications:" is present
    Then User Verifies "FileControlsHeaderPage|SafeguardUsage,Safeguard Usage:" is present
    Then User Verifies "FileControlsHeaderPage|AutoApproveBenefits,Auto Approve Benefits:" is present
    Then User Verifies "FileControlsHeaderPage|CreationDate,Creation Date:" is present
    Then User Verifies "FileControlsHeaderPage|LoginIdMethod,LoginId Method:" is present
    Then User Verifies "FileControlsHeaderPage|CreationTime,Creation Time:" is present
    Then User Verifies "FileControlsHeaderPage|ProcessingResults,Processing Results" is present
    Then User Verifies "FileControlsHeaderPage|ExceptionManagement,Exception Management" is present
    Then User Verifies "FileControlsHeaderPage|LoadStatus,Load Status" is present
    Then User Verifies "FileControlsHeaderPage|LoadDetails,Load Details" is present
    Then User Verifies "FileControlsHeaderPage|ControlResults,Control Results" is present
    Then User Verifies "FileControlsHeaderPage|TaskResults,Task Results" is present


