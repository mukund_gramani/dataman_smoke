Feature: Smoke Scenarios for DataMan

  @OriginalFile

  Scenario: Verify Original File Page

    Given User Navigate to DataMan Home Page
    When User navigates to Clients tab
    And User enters client name QALIFE
    Then User clicks on "ParticipantSearchPage|Searchbtn"
    Then User clicks on "ParticipantSearchPage|QA62BC"
    Then User clicks on "SelectedFileLinks|DataManIFStatus"
    Then User selects a file
    Then User clicks on "IFSLSelectedFile|FileDetails"
    Then User clicks on "IFSLSelectedFile|ManageExceptionsbtn"
    Then User clicks on "SelectedFileLinks|OriginalFileLink"
    Then User clicks on "SelectedFileLinks|ExportData"
    Then User Verifies file is downloaded

