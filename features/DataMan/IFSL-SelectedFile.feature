Feature: Smoke Scenarios for DataMan

  @SelectedFile
  Scenario: Verify IFSL Selected File Links
    Given User Navigate to DataMan Home Page
    When User navigates to Clients tab
    And User enters client name QALIFE
    Then User clicks on "ParticipantSearchPage|Searchbtn"
    Then User clicks on "ParticipantSearchPage|QA62BC"
    Then User clicks on "SelectedFileLinks|DataManIFStatus"
    Then User selects a file with EM task
    Then User clicks on "IFSLSelectedFile|FileDetails"
    Then User clicks on "IFSLSelectedFile|ManageExceptionsbtn"
    Then User Verifies "SelectedFileLinks|ManageExceptionsLink,Manage Exceptions" is present
    Then User clicks on "SelectedFileLinks|ManageExceptionsLink"
    Then User Verifies "Validations1|ManageExceptionsValidn,Manage Exceptions" is present
    Then User Verifies "SelectedFileLinks|PeerReviewLink,Peer Review" is present
    Then User Verifies "SelectedFileLinks|OriginalFileLink,Original File Data" is present
    Then User Verifies "SelectedFileLinks|ViewFileControlsLink,View File Controls" is present
    Then User Verifies "SelectedFileLinks|ViewProcessedDataLink,View Processed Data" is present
    Then User clicks on "SelectedFileLinks|PeerReviewLink"
    Then User Verifies "Validations1|PeerReviewValidn,Peer Review" is present
    Then User clicks on "SelectedFileLinks|ViewProcessedDataLink"
    Then User Verifies "Validations1|ViewProcessedDataValidn,View Processed Data" is present




