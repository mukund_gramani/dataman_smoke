module.exports = {
    sections: {
        HomePage: {
            selector: 'body',
            elements: {
                HomeTab: {
                    locateStrategy: 'xpath',
                    selector: '//strong[contains(text(),"Home")]'
                },
                MercerCopyright: {
                    locateStrategy: 'xpath',
                    selector: '//td[contains(text(),"© Copyright 2018 Mercer ")]'
                },
                WelcomeTitle: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Welcome")]'
                },
                MercerLogo: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="WebPart_twp181066602"]/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td/table/tbody/tr[1]/td[1]/table/tbody/tr/td/a/img'
                },

                LoginUsername: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="WebPart_twp181066602"]/tbody/tr/td/table/tbody/tr/td/table/tbody/tr[1]/td/table/tbody/tr[1]/td[3]/table/tbody/tr[2]/td[1]'
                },
                RecentClients: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Recent Clients")]'
                },
                RecentParticipants: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Recent Participants")]'
                },

            }
        },
        ParticipantSearchPage: {
            selector: 'body',
            elements: {
                ClientsTab: {
                    locateStrategy: 'xpath',
                    selector: '//strong[contains(text(),"Clients")]'
                },
                ClientNm: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Client Name /One Code" )]'
                },
                ClientNm_xpath: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="WebPart_twp1709515677"]/tbody/tr[2]/td/table[1]/tbody/tr[2]/td/input[1]'
                },
                Searchbtn: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="WebPart_twp1709515677"]/tbody/tr[2]/td/table[2]/tbody/tr[2]/td/input'
                },

                QA62BC: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="TopazWebPartManager1_twp755100566"]/tbody/tr[4]/td/div/table/tbody/tr[2]/td[2]/a'
                },
                DMInboundFileStatus: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="TopazWebPartManager1_twp634636190_ctl05_TABLE_SERVICE_OPTION_NAME_LABEL_1"]'
                },

                InboundFileStatusList: {
                    locateStrategy: 'xpath',
                    selector: '//div[contains(text(),"Inbound File Status List")]'
                },

                DMConfiguration: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Data Man Configuration")]'
                },
                ClientTransmissionLocationList: {
                    locateStrategy: 'xpath',
                    selector: '//div[contains(text(),"Client Transmission Location List")]'
                },
                FileConfigList: {
                    locateStrategy: 'xpath',
                    selector: '//div[contains(text(),"File Configuration List")]'
                },
                PullServerConfigList: {
                    locateStrategy: 'xpath',
                    selector: '//div[contains(text(),"Pull Server Configuration List")]'
                },
                DataManConfiguration: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="WebPart_tgwp585531037"]/tbody/tr/td/table[2]/tbody/tr/td[1]'
                },
                InboundFileControlPanel: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Inbound File Control Panel")]'
                },
                MergeTableDelete: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Merge Table Delete")]'
                },
                AutomatedBWLoadStatusReport: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Automated BW Load Status Report")]'
                },
                IFSList: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="WebPart_tgwp1784701154"]/tbody/tr/td/span/a'
                },


            }
        },
        Validations: {
            selector: 'body',
            elements: {
                IFCP: {
                    locateStrategy: 'xpath',
                    selector: '//div[contains(text(),"Inbound File Control Panel")]'
                },
                MTD: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="WebPart_tgwp1017735246"]/tbody/tr/td/div[1]'
                },
                ABWLSR: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="WebPart_tgwp1784701154"]/tbody/tr/td/div[2]'
                },
                InboundFSList: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="WebPart_tgwp1345456082"]/tbody/tr/td/div[2]'
                },

            }
        },
        TopNavQuickSearch: {
            selector: 'body',
            elements: {
                ClientsDD: {
                    locateStrategy: 'xpath',
                    selector: '//option[contains(text(),"Clients")]'
                },
                ParticipantsDD: {
                    locateStrategy: 'xpath',
                    selector: '//option[contains(text(),"Participants")]'
                },
                AdministratorsDD: {
                    locateStrategy: 'xpath',
                    selector: '//option[contains(text(),"Administrators")]'
                },
                btnSearchAllPgs: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="WebPart_twp991671374"]/tbody/tr/td/div/table/tbody/tr[2]/td[1]/input'
                },
                ClientsTab: {
                    locateStrategy: 'xpath',
                    selector: '//strong[contains(text(),"Clients")]'
                },
                ParticipantsTab: {
                    locateStrategy: 'xpath',
                    selector: '//strong[contains(text(),"Participants")]'
                },
                AdministrationTab: {
                    locateStrategy: 'xpath',
                    selector: '//strong[contains(text(),"Administration")]'
                },
                PptLastName: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="WebPart_twp1215609112"]/tbody/tr[2]/td/table/tbody/tr[4]/td/table/tbody/tr[2]/td[2]/input'

                },
                CountryCode: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="WebPart_twp1215609112"]/tbody/tr[2]/td/table/tbody/tr[4]/td/table/tbody/tr[7]/td[2]/select'

                },
                PptBtnSearch: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="WebPart_twp1215609112"]/tbody/tr[2]/td/table/tbody/tr[4]/td/table/tbody/tr[11]/td[2]/input'

                },
            }


        },
        PptSearchResultPage: {
            selector: 'body',
            elements: {
                PptSearchRes: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="TopazWebPartManager1_twp1787184011_displayGrid"]/tbody/tr[2]/td[1]/a'
                },
                Dropdown: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="WebPart_twp991671374"]/tbody/tr/td/div/table/tbody/tr[1]/td[2]/select'
                },

                QuickSearch: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="WebPart_twp991671374"]/tbody/tr/td/div/table/tbody/tr[2]/td[1]/input'
                },

                SearchbtnParticipants: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="WebPart_twp991671374"]/tbody/tr/td/div/table/tbody/tr[2]/td[2]/input'
                },
                ResTable: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="TopazWebPartManager1_twp755100566"]/tbody/tr[5]/td/div/table'
                },


            }
        },
        IFSLSelectedFile: {
            selector: 'body',
            elements: {
                FA: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="TopazWebPartManager1_tgwp1345456082_twp1345456082_cbFileApproval"]'
                },
                PR: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="TopazWebPartManager1_tgwp1345456082_twp1345456082_cbPeerReview"]'
                },
                BW: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="TopazWebPartManager1_tgwp1345456082_twp1345456082_cbBWLoad"]'
                },
                BS: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="TopazWebPartManager1_tgwp1345456082_twp1345456082_cbBCStageLoad"]'
                },
                BC: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="TopazWebPartManager1_tgwp1345456082_twp1345456082_cbBCProdLoad"]'
                },
                NP: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="TopazWebPartManager1_tgwp1345456082_twp1345456082_cbNothingPending"]'
                },
                IP: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="TopazWebPartManager1_tgwp1345456082_twp1345456082_cbInProgress"]'
                },
                Failed: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="TopazWebPartManager1_tgwp1345456082_twp1345456082_cbFailed"]'
                },
                ApplyFiltersbtn: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="TopazWebPartManager1_tgwp1345456082_twp1345456082_btnApplyFilter"]'
                },
                FileDetails: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="TopazWebPartManager1_tgwp1345456082_twp1345456082_grid"]/tbody/tr[2]/td[6]'
                },
                ManageExceptionsbtn: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="TopazWebPartManager1_tgwp1345456082_twp1345456082_btnManageExceptions"]'

                },

            }
        },
        SelectedFileLinks: {
            selector: 'body',
            elements: {
                AutomatedBWLoadStatusLink: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="TopazWebPartManager1_twp184578419"]/table[1]/tbody/tr/td[1]/ul/li/a'
                },
                ManageExceptionsLink: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Manage Exceptions")]'
                },
                OriginalFileLink: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Original File Data")]'
                },
                PeerReviewLink: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Peer Review")]'
                },
                ExportData: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="TopazWebPartManager1_tgwp708491670_twp708491670_btnExportData"]'
                },
                ViewFileControlsLink: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"View File Controls")]'
                },
                DataManIFStatus: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Data Man Inbound File Status")]'
                },
                ViewProcessedDataLink: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"View Processed Data")]'
                },

            }
        },
        Validations1: {
            selector: 'body',
            elements: {
                ManageExceptionsValidn: {
                    locateStrategy: 'xpath',
                    selector: '//div[contains(text(),"Manage Exceptions")]'
                },
                PeerReviewValidn: {
                    locateStrategy: 'xpath',
                    selector: '//div[contains(text(),"Peer Review")]'
                },
                ViewProcessedDataValidn: {
                    locateStrategy: 'xpath',
                    selector: '//div[contains(text(),"View Processed Data")]'
                }
            }
        },
        FileControlsHeaderPage: {
            selector: 'body',
            elements: {
                FileCtrls: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="TopazWebPartManager1_twp184578419"]/table[5]/tbody/tr/td[1]/ul/li/a'
                },
                DateReceived: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Date Received:")]'
                },
                FileID: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"File ID:")]'
                },
                FileName: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"File Name:")]'
                },
                ConfigID: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Config ID:")]'
                },
                AltPath: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Alt Path:")]'
                },
                FileType: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"File Type:")]'
                },
                FileStatus: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"File Status:")]'
                },
                Loaded: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Loaded:")]'
                },

                ProcessingBy: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Processing By:")]'
                },

                OriginalAmt: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Original Amt:")]'
                },

                LastUpdated: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Last Updated:")]'
                },
                EnabledApplications: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Enabled Applications:")]'
                },
                SafeguardUsage: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Safeguard Usage:")]'
                },
                CreationDate: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Creation Date:")]'
                },
                AutoApproveBenefits: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Auto Approve Benefits:")]'
                },
                LoginIdMethod: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"LoginId Method:")]'
                },
                CreationTime: {
                    locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Creation Time:")]'
                },
                ProcessingResults: {
                    locateStrategy: 'xpath',
                    selector: '//div[contains(text(),"Processing Results")]'
                },
                ExceptionManagement: {
                    locateStrategy: 'xpath',
                    selector: '//div[contains(text(),"Exception Management")]'
                },
                LoadStatus: {
                    locateStrategy: 'xpath',
                    selector: '//div[contains(text(),"Load Status")]'
                },
                TaskResults: {
                    locateStrategy: 'xpath',
                    selector: '//div[contains(text(),"Task Results")]'
                },
                ControlResults: {
                    locateStrategy: 'xpath',
                    selector: '//div[contains(text(),"Control Results")]'
                },
                LoadDetails: {
                    locateStrategy: 'xpath',
                    selector: '//div[contains(text(),"Load Details")]'
                },
                TimePeriodFilter: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="TopazWebPartManager1_tgwp1345456082_twp1345456082_cmbTimePeriod"]'
                },
                Last24Hrs: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="TopazWebPartManager1_tgwp1345456082_twp1345456082_cmbTimePeriod"]/option[2]'
                },
                Last7Days: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="TopazWebPartManager1_tgwp1345456082_twp1345456082_cmbTimePeriod"]/option[3]'

                },
                Last30Days: {
                    locateStrategy: 'xpath',
                    selector: '//*[@id="TopazWebPartManager1_tgwp1345456082_twp1345456082_cmbTimePeriod"]/option[4]'
                },

            }
        }
    }
};